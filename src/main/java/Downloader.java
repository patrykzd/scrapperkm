import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

class Downloader {
    private static int orderNonNamedNumber = 0;

    public static void download(String link, String path) {
        File out = new File(path);
        BufferedInputStream in = null;
        BufferedOutputStream bout = null;
        FileOutputStream fos = null;
        try {
            URL url = new URL(link);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(http.getInputStream());
            fos = new FileOutputStream(out);
            bout = new BufferedOutputStream(fos, 1024);
            byte[] buffer = new byte[1024];
            int read = 0;

            while ((read = in.read(buffer, 0, 1024)) >= 0) {
                bout.write(buffer, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
                bout.close();
                in.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void download(String link) {
        download(link, System.getProperty("user.dir") + "\\" + orderNonNamedNumber);
        orderNonNamedNumber++;
    }


}
