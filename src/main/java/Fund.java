import java.io.Serializable;

public class Fund implements Serializable {
    private static final long serialVersionUID = 3812017177088226528L;
    public Document KIID;
    private final String ISIN;

    public Fund(String ISIN, String date) {
        this.ISIN = ISIN;
        this.KIID = new KIID(date);
    }

    public Fund(String ISIN) {
        this(ISIN, "1800-01-01");
    }

    public Fund() {
        this("", "1800-01-01");
    }

    public String getISIN() {
        return ISIN;
    }

}
