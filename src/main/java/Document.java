import java.io.Serializable;
import java.time.LocalDate;
import java.util.Optional;

abstract class Document implements Serializable {

    private LocalDate date;

    public Document(String date) {
        this.date = LocalDate.parse(date);
    }

    // http://www.java2novice.com/java-8/date-time-api/compare-date-objects/ - sorry :P
    public static boolean isNewer(String potentialDate, String documentDate) {
        return LocalDate.parse(potentialDate).isAfter(LocalDate.parse(documentDate));
    }

    //    you could do this with one liner :p this.date = Optional.of(LocalDate.of(potencialNewDate)).filter(d -> d.isAfter(this.date)).orElse(this.date);
    public void setNewDocumentDate(String potentcialNewDate) {
        this.date = Optional.of(LocalDate.parse(potentcialNewDate)).filter(d -> d.isAfter(this.date)).orElse(this.date);
    }

    public String getDocDate() {
        return String.valueOf(date);
    }

}

