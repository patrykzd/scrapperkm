import org.jsoup.Jsoup;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Scrapper {
    private WebDriverWait wd;
    private WebDriver driver;
    private Downloader dl;
    private String path;

    public Scrapper() {
        this.init();
    }

    private void init() {

        this.path = System.getProperty("user.dir");
        this.dl = new Downloader();
        String ISIN = "ie00b4l5y983";
        final String query = "https://www.fundinfo.com/en/IE-prof/LandingPage?query=";
        final String url = query + ISIN;
        Map<String, String> cookies = Stream.of("_ga=GA1.2.1069795860.1561922489; PrivacyPolicy=true; PreferredLanguage=en; _gid=GA1.2.1631744730.1562604484; DU=IE-prof; BIGipServerwww.fundinfo.com=950034711.47873.0000; _gat_gtag_UA_3567644_1=1; ".split("; ")).map(e -> e.split("=")).collect(Collectors.toMap(e -> e[0], e -> e[1]));

        try {
            cookies.putAll(Jsoup.connect(url).execute().cookies());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ChromeOptions c = new ChromeOptions();
//        c.addArguments("--headless");

        this.driver = new ChromeDriver(c);
        this.driver.manage().window().maximize();

        this.driver.get(url);
        cookies.forEach((k, v) -> driver.manage().addCookie(new Cookie(k, v)));
        this.driver.get(url);
        this.wd = new WebDriverWait(driver, 50);
    }

    void loopSearch(Fund fund) {
        this.wd.until(d -> (d.findElement(By.cssSelector("body > div.wait")).getAttribute("style").equals("border-top-color: rgb(255, 102, 0); border-bottom-color: rgb(255, 102, 0); display: none;")));

        try {
            Thread.sleep(250);
            driver.findElement(By.cssSelector(".cells.row")).findElements(By.cssSelector(".cellContent.cell .pdf-container.member-only"));
            documentUrlResolver(fund);
        } catch (NoSuchElementException | InterruptedException e) {

            try {
                System.out.println("searching for button alt");
                driver.findElement(By.cssSelector("div[style='display: table;'] > ul > li > div > a > button.btn.active"));
                altDocumentUrlResolver(fund);
            } catch (NoSuchElementException y) {
                loopSearch(fund);
            }
        }
    }

    private void documentUrlResolver(Fund fund) {
//        body > div.wrapper.footerSpace > div > div.results-container > div.table-container.GridControl > div > div:nth-child(2) > ul > li > div > div.cells.row > div:nth-child(6) > div > div.pdf-container.member-only > div.pdf-popover > ul > li > a
//        ((JavascriptExecutor)this.driver).executeScript("window.interval = setInterval(function () {window.a = document.querySelector('div.results-container > div.table-container.GridControl > div > div:nth-child(2) > ul > li > div > div.cells.row') || document.querySelector('div[style=\"display: table;\"] > ul > li > div > a > button.btn.active');if (window.a !== null) {console.log(window.a);clearInterval(window.interval);return;}}, 250);");

        WebElement checkElem = this.wd.until(d -> d.findElement(By.cssSelector("div.pdf-container.member-only > div.icon-ic_pdf.Orange > div")));
        if (!(checkElem.getAttribute("innerHTML").toUpperCase().equals("?")) && !(checkElem.getAttribute("innerHTML").toUpperCase().equals("-"))) {

            ((JavascriptExecutor) this.driver).executeScript("FundGrid.popOver(document.querySelector(\"div[data-name=KID]\"),'D');");
            WebElement e = this.wd.until(d -> d.findElement(By.cssSelector("div.cells.row > div:nth-child(6) > div > div.pdf-container.member-only > div.pdf-popover > ul > li > a")));
            Pattern pattern = Pattern.compile("\\>(.*?)\\<");
            String htmlString = e.getAttribute("innerHTML");
            String link = e.getAttribute("href");
            Matcher m = pattern.matcher(htmlString);
            String text = "";

            while (m.find()) {
                text += htmlString.substring(m.start() + 1, m.end() - 1);
                text += " ";
            }
            text = text.replace("  ", "");
            text = text.substring(1);

            Pattern docDatePattern = Pattern.compile("\\d\\d\\d\\d-\\d\\d-\\d\\d");
            m = docDatePattern.matcher(text);
            m.find();
            String docDate = text.substring(m.start(), m.end());

            if (Document.isNewer(docDate, fund.KIID.getDocDate())) {
                fund.KIID.setNewDocumentDate(docDate);
                System.out.println(fund.getISIN() + " doc downloaded " + docDate);
            this.dl.download(link, this.path + "\\" + text +" " + fund.getISIN() + ".pdf" );
            }
        }
    }

    private void altDocumentUrlResolver(Fund fund) {
        WebElement altButton = this.wd.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.wrapper.footerSpace > div > div.results-container > div.table-container.GridControl > div > div:nth-child(2) > ul > li > div > a > button")));
        altButton.click();
        loopAfterSearch(fund);
    }

    void loopAfterSearch(Fund fund) {
        try {
            Thread.sleep(250);
            this.driver.findElement(By.cssSelector("body > div.wrapper.footerSpace > div > div > div > a"));
            List<WebElement> elements = this.driver.findElements(By.cssSelector("body > div.wrapper.footerSpace > div > div > div > a"));
            WebElement elem = null;
            for (WebElement e : elements) {
                if (e.getAttribute("innerHTML").contains("Great Britain")) {
                    elem = e;
                    break;
                }
            }
            if (elem == null) {
                elem = elements.get(0);
            }
            this.wd.until(ExpectedConditions.elementToBeClickable(elem)).click();
            listDocResolver(fund);
        } catch (NoSuchElementException | InterruptedException e) {
            try {
                driver.findElement(By.cssSelector("body > div.wrapper.footerSpace > div > div > h3"));
            } catch (NoSuchElementException y) {
                loopSearch(fund);
            }
        }
    }

    private void listDocResolver(Fund fund) {
        try {
            Thread.sleep(250);
            this.wd.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for=fund-market-checkbox-mandatory] > span"))).click();
            this.wd.until(ExpectedConditions.elementToBeClickable(By.cssSelector("body > header > div.fund-market-wrapper.active > div.fund-market-modal-container > div > div.fund-market-picker-container > div > div:nth-child(7) > button.btn.fundmarket-btn.confirm"))).click();
        } catch (ElementClickInterceptedException | InterruptedException e) {
            listDocResolver(fund);
        }
        loopSearch(fund);
    }

    private void nextFundSearch(Fund fund) {
        WebElement input = this.wd.until(d -> d.findElement(By.cssSelector("input")));
        setAttribute(input, "value", fund.getISIN());
        WebElement searchButton = this.driver.findElement(By.cssSelector("button.searchButton"));
        searchButton.click();
    }

    private void setAttribute(WebElement element, String attName, String attValue) {
        ((JavascriptExecutor) this.driver).executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                element, attName, attValue);
    }

    public void goScrap(Fund fund) {
        nextFundSearch(fund);
        loopSearch(fund);
    }

    public void endScraping() {
        this.driver.quit();
    }
}
