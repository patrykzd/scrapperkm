import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        FileManager fileManager = new FileManager();
        ArrayList<Fund> fundsListToProcess = fileManager.getDataFromExcel();
        if (fundsListToProcess != null) {
            HashMap<String, Fund> readedFundList = fileManager.readDataFromFile();
            fundsListToProcess.forEach(e -> {
                if (readedFundList.containsKey(e.getISIN())) {
                    e.KIID.setNewDocumentDate(readedFundList.get(e.getISIN()).KIID.getDocDate());
                }
            });

            Scrapper scrapper = new Scrapper();
            int i = 0;

            while (i < fundsListToProcess.size() && i < 10) {
                scrapper.goScrap(fundsListToProcess.get(i));
                i++;
            }

            scrapper.endScraping();
            fundsListToProcess.forEach(e -> {
                readedFundList.put(e.getISIN(), e);
            });

            fileManager.exportDataToFile(readedFundList);
        }
    }
}
