import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class FileManager {

    public void exportDataToFile(HashMap<String, Fund> arr) {

        try (FileOutputStream fs = new FileOutputStream("funds.obj");
             ObjectOutputStream os = new ObjectOutputStream(fs)) {
            os.writeObject(arr);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public HashMap<String, Fund> readDataFromFile() {
        String fileName = "funds.obj";
        HashMap<String, Fund> fundList = new HashMap<>();

        try (
                FileInputStream fis = new FileInputStream(fileName);
                ObjectInputStream ois = new ObjectInputStream(fis)
        ) {
            fundList = (HashMap<String, Fund>) ois.readObject();
        } catch (ClassNotFoundException | IOException | ClassCastException e) {
                e.printStackTrace();
        }
        return fundList;
    }

    ArrayList<Fund> getDataFromExcel(String path) {
        ArrayList<Fund> fundList = new ArrayList<>();
        try (InputStream inp = new FileInputStream(path)) {
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            int sheetRowsNumber = sheet.getLastRowNum() + 1;

            for (int i = 0; i < sheetRowsNumber; i++) {
                Row row = sheet.getRow(i);
                Cell cell = row.getCell(0);
                fundList.add(new Fund(cell.getStringCellValue()));
            }

        } catch (InvalidFormatException | IOException e) {
            e.printStackTrace();
            return null;
        }
        return fundList;
    }

    ArrayList<Fund> getDataFromExcel() {
        return getDataFromExcel(System.getProperty("user.dir") + "/Zeszyt1.xlsx");
    }

}

